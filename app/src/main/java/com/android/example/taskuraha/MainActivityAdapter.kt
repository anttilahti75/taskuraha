package com.android.example.taskuraha

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recycler_item.view.*


class MainActivityAdapter internal constructor(context: Context, val viewModel: BalanceViewModel) : RecyclerView.Adapter<MainActivityAdapter.ItemViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var balanceItems = emptyList<Balance>()

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val dateTextView: TextView = view.dateTextView
        val balanceTextView: TextView = view.balanceTextView
        //val diffToPrevBalTextView: TextView = view.diffToPrevBalTextView
        val deleteItem: TextView = view.deleteBalanceTextView
        var itemBox = view.itemBox
        var context = view.context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val retView = inflater.inflate(R.layout.recycler_item, parent, false)
        return ItemViewHolder(retView)
    }

    override fun getItemCount(): Int {
        return balanceItems.size
    }

    internal fun setBalanceItems(balanceItems: List<Balance>) {
        this.balanceItems = balanceItems
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val current = balanceItems[position]
        holder.dateTextView.text = current.date
        holder.balanceTextView.text = current.balance.toString()

        // Button, "X", deletes balance item
        holder.deleteItem.setOnClickListener {
            viewModel.delete(current)
            Toast.makeText(holder.context, "Balance deleted succesfully", Toast.LENGTH_LONG).show()
        }

        // Clicking the whole balance item box allows to edit the item
        holder.itemBox.setOnClickListener {
            val extras = Bundle()
            extras.putString("msg", "updateItem")
            extras.putString("date", current.date)
            extras.putDouble("balance", current.balance)
            extras.putString("items", current.items)
            extras.putInt("id", current.id)
            val intent = Intent(holder.context, AddNewBalanceItem::class.java).apply {
                putExtras(extras)
            }
            holder.context.startActivity(intent)
        }
    }

}