package com.android.example.taskuraha

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {

    private lateinit var balanceViewModel: BalanceViewModel
    private lateinit var adapter: MainActivityAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        balanceViewModel = ViewModelProvider(this).get(BalanceViewModel::class.java)
        balanceViewModel.allBalances.observe(this, Observer { balances ->
            // Update the cached copy of the balances in the adapter.
            balances?.let { adapter.setBalanceItems(it) }
        })

        // Recyclerview
        val recyclerView = findViewById<RecyclerView>(R.id.balanceRecyclerView)
        adapter = MainActivityAdapter(this, balanceViewModel)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        // Button for adding new balance item
        val fab = findViewById<FloatingActionButton>(R.id.floatingActionButton)
        fab.setOnClickListener {
            val extras = Bundle()
            extras.putString("msg", "newItem")
            val intent = Intent(this, AddNewBalanceItem::class.java).apply {
                putExtras(extras)
            }
            startActivity(intent)
        }

        // Button to get to the chart activity
        val chartFab = findViewById<FloatingActionButton>(R.id.showChartFab)
        chartFab.setOnClickListener {
            val extras = Bundle()
            extras.putString("dates", "")
            val intent = Intent(this, ChartsActivity::class.java)
            startActivity(intent)
        }

        // Shows total balance at top of the screen
        val totalBal = findViewById<TextView>(R.id.totalBalanceTextView)
        balanceViewModel.getBalanceTotal.observe(this, Observer {
                bal -> if(bal != null) { totalBal.text = "Total: " + bal.toString() }
                        else { totalBal.text = "Total: 0.0" }
                //if (bal > 0) { totalBal.setTextColor(Color.GREEN) }
        })
    }
}
