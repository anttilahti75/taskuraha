package com.android.example.taskuraha

import android.app.Application
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

class BalanceViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: BalanceRepository

    val allBalances: LiveData<List<Balance>>
    val getBalanceTotal: LiveData<Double>

    init {
        val balanceDAO = BalanceRoomDatabase.getDatabase(application).balanceDao()
        repository = BalanceRepository(balanceDAO)
        allBalances = repository.allBalanceItems
        getBalanceTotal = repository.getBalanceTotal
    }


    fun insert(id: Int, year: Int, month: Int, day: Int, items: String) = viewModelScope.launch(Dispatchers.IO) {
        val curId = repository.insert(id, year, month, day, items)
        updateAll(curId)
    }

    /*
    suspend fun insert(id: Int, year: Int, month: Int, day: Int, items: String): Long = coroutineScope {
        val newId = async { repository.insert(id, year, month, day, items) }
        newId.await()
    } */


    suspend fun update(balanceItem: Balance, year: Int, month: Int, day: Int) = viewModelScope.launch(Dispatchers.IO) {
        val curId = repository.update(balanceItem, year, month, day)
        updateAll(curId.toLong())
    }

    /*
    suspend fun update(balanceItem: Balance, year: Int, month: Int, day: Int) = coroutineScope {
        val curId = async { repository.update(balanceItem, year, month, day) }
        curId.await()
    } */

    fun delete(balanceItem: Balance) = viewModelScope.launch(Dispatchers.IO) {
        repository.delete(balanceItem)
    }

    suspend fun updateAll(newId: Long) = coroutineScope {
        val list = getBalanceItemsInAList().filter { x -> x.id > newId }
        list.forEach {
            val parts = it.date.toString().split("-")
            repository.update(it, parts[0].toInt(), parts[1].toInt(), parts[2].toInt())
        }
    }

    suspend fun getBalanceItemsInAList(): List<Balance> = coroutineScope {
        val data = async {repository.getBalanceItemsInAList()}
        data.await()
    }
}