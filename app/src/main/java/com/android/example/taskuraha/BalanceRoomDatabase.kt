package com.android.example.taskuraha

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = arrayOf(Balance::class), version = 7, exportSchema = false)
    abstract class BalanceRoomDatabase : RoomDatabase() {

    abstract fun balanceDao(): BalanceDao

    companion object {
        @Volatile
        private var INSTANCE: BalanceRoomDatabase? = null

        fun getDatabase(context: Context): BalanceRoomDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            } else {
                synchronized(this) {
                    val instance = Room.databaseBuilder(
                        context.applicationContext,
                        BalanceRoomDatabase::class.java,
                        "balance_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                    return instance
                }
            }
        }
    }
}