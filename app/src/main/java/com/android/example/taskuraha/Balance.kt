package com.android.example.taskuraha

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "balance_table")
data class Balance(

    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,

    val date: String = "",

    val items: String = ""
) {
    var balance: Double = 0.0
}