package com.android.example.taskuraha

import androidx.lifecycle.LiveData

class BalanceRepository(private val balanceDao: BalanceDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allBalanceItems: LiveData<List<Balance>> = balanceDao.getBalanceItems()

    suspend fun insert(id: Int, year: Int, month: Int, day: Int, items: String): Long {
        val date = "$year-$month-$day"
        val prevDay = day - 1
        val prevDate = "$year-$month-$prevDay"
        val bal = prevBal(prevDate) + countItemTotal(items)
        val newBalanceItem = Balance(id, date, items)
        newBalanceItem.balance = bal
        return balanceDao.insert(newBalanceItem)
    }

    suspend fun update(balanceItem: Balance, year: Int, month: Int, day: Int): Int {
        val prevDay = day - 1
        val prevDate = "$year-$month-$prevDay"
        val bal = prevBal(prevDate) + countItemTotal(balanceItem.items)
        balanceItem.balance = bal
        return balanceDao.update(balanceItem)
    }

    suspend fun delete(balanceItem: Balance) {
        balanceDao.deleteBalance(balanceItem)
    }

    suspend fun deleteAll() {
        balanceDao.deleteAll()
    }

    suspend fun getBalanceItemsInAList(): List<Balance> {
        return balanceDao.getBalanceItemsInAList()
    }

    val getBalanceTotal: LiveData<Double> = balanceDao.getBalanceTotal()

    private fun countItemTotal(items: String): Double {
        var total = 0.0
        if (items.length != 0) {
            val itemArr = items.split(",")
            for (x in 0 until itemArr.count()) {
                if (!itemArr[x].isEmpty()) {
                    total += itemArr[x].toDouble()
                }
            }
        }
        return total
    }

    fun prevBal(date: String): Double {
        val prev = balanceDao.getBalanceByDate(date)
        return prev
    }
}