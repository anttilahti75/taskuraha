package com.android.example.taskuraha

import android.app.DatePickerDialog
import android.icu.util.Calendar
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.activity_add_new_balance_item.*
import kotlinx.android.synthetic.main.new_item.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class AddNewBalanceItem() : AppCompatActivity() {

    private lateinit var balanceViewModel: BalanceViewModel
    private lateinit var inflater: LayoutInflater
    private var balanceItems = mutableListOf<View>()
    val c = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_balance_item)

        balanceViewModel = ViewModelProvider(this).get(BalanceViewModel::class.java)
        inflater = LayoutInflater.from(this)

        intent = getIntent();
        val extras = intent.getExtras()

        // Button, accept the items which make the whole balance item
        // Action depends on intent bundle; either "newItem" or "updateItem"
        val biofab = findViewById<FloatingActionButton>(R.id.balanceItemOkFab)

        // Button, add a new item to the balance item
        val binfab = findViewById<FloatingActionButton>(R.id.balanceItemNewFab)
        binfab.setOnClickListener {
            addNewItem(addNewBalanceItemLayout)
        }

        if (extras?.getString("msg") == "newItem") {
            dateTextView.text = getString(R.string.balanceItemDateTextView, c.get(Calendar.YEAR), c.get(Calendar.MONTH)+1, c.get(Calendar.DAY_OF_MONTH))

            biofab.setOnClickListener {
                // Split date to individual components
                val parts = dateTextView.text.toString().split("-")

                // id is 0 for autogenerator purposes
                val id = 0

                GlobalScope.launch {
                    balanceViewModel.insert(
                        id,
                        parts[0].toInt(),
                        parts[1].toInt(),
                        parts[2].toInt(),
                        joinEditTextStrings(balanceItems)
                    )
                }

                Toast.makeText(this, "Balance added successfully", Toast.LENGTH_LONG).show()
                this.finish()
            }

        } else if (extras?.getString("msg") == "updateItem") {
            addNewBalanceTitle.text = "Modify balance"

            val date = extras.getString("date")
            val id = extras.getInt("id")
            val items = extras.getString("items").toString().split(",")
            val balance = extras.getDouble("balance").toString()
            for (i in 0 until items.count()) {
                if (items[i] != "") {
                    addNewItem(addNewBalanceItemLayout)
                    balanceItems[i].editText.setText(items[i])
                }
            }
            dateTextView.text = date

            biofab.setOnClickListener {
                val parts = dateTextView.text.toString().split("-")
                val newBalItem = Balance(id, dateTextView.text.toString(), joinEditTextStrings(balanceItems))

                GlobalScope.launch {
                    balanceViewModel.update(newBalItem, parts[0].toInt(), parts[1].toInt(), parts[2].toInt())
                }
                Toast.makeText(this, "Balance updated successfully", Toast.LENGTH_LONG).show()
                this.finish()
            }
        }
    }

    fun showDatePickerDialog(view: View) {
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            dateTextView.text = getString(R.string.balanceItemDateTextView, year, monthOfYear+1, dayOfMonth)
        }, year, month, day)
        dpd.show()
    }

    fun addNewItem(parent: ViewGroup): Int {
        val newItemId = View.generateViewId()
        val newItem = inflater.inflate(R.layout.new_item, parent, false)
        newItem.id = newItemId
        newItem.deleteItem.setOnClickListener {
            scrollviewContainer.removeView(newItem)
            balanceItems = balanceItems.filter { i -> i.id != newItemId }.toMutableList()
        }

        scrollviewContainer.addView(newItem)
        balanceItems.add(newItem)

        return newItemId
    }

    fun joinEditTextStrings(list: MutableList<View>): String {
        var balItems = ""
        for (i in 0 until list.count()) {
            val text = list[i].editText.text.toString()
            balItems += "$text,"
        }
        return balItems
    }
}
