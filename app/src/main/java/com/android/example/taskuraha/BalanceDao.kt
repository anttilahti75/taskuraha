package com.android.example.taskuraha

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.*

@Dao
interface BalanceDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(balance: Balance): Long

    @Update
    suspend fun update(balance: Balance): Int

    @Delete
    suspend fun deleteBalance(balance: Balance)

    @Query("DELETE FROM balance_table")
    suspend fun deleteAll()

    @Query("SELECT * FROM balance_table ORDER BY date ASC")
    fun getBalanceItems(): LiveData<List<Balance>>

    @Query("SELECT SUM(balance) FROM balance_table")
    fun getBalanceTotal(): LiveData<Double>

    @Query("SELECT * FROM balance_table WHERE id = :id")
    fun getBalanceById(id: Int): Balance

    /*
    @Query("SELECT * FROM balance_table WHERE date = :date")
    fun getBalanceByDate(date: String): LiveData<Balance>
    */

    @Query("SELECT items FROM balance_table WHERE date = :date")
    fun getBalanceItemsByDate(date: String): String

    @Query("SELECT balance FROM balance_table WHERE date = :date")
    fun getBalanceByDate(date: String): Double

    @Query("SELECT * FROM balance_table ORDER BY date ASC")
    suspend fun getBalanceItemsInAList(): List<Balance>
}