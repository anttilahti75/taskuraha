package com.android.example.taskuraha

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.*
import kotlinx.android.synthetic.main.activity_charts.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ChartsActivity() : AppCompatActivity() {

    private lateinit var balanceItems: List<Balance>
    private lateinit var balanceViewModel: BalanceViewModel
    private var chartItems = mutableListOf<BarEntry>()
    private val months = listOf(
        "January", "February", "March",
        "April", "May", "June",
        "July", "August", "September",
        "October", "November", "December"
    )
    private var curMonth = 0
    private var curYear = 2020

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_charts)

        intent = getIntent()

        balanceViewModel = ViewModelProvider(this).get(BalanceViewModel::class.java)
        val chart: BarChart = findViewById(R.id.chart)


        GlobalScope.launch {
            balanceItems = balanceViewModel.getBalanceItemsInAList()

            getMonthData(curYear, curMonth)
            chartMonthTextViewButton.text = months[curMonth]
            chartYearTextViewButton.text = curYear.toString()
        }

        chartMonthTextViewButton.setOnClickListener {

        }

        prevMonthButton.setOnClickListener {
            if (curMonth == 0) {
                curMonth = 11
            } else {
                curMonth -= 1
            }
            chartMonthTextViewButton.text = months[curMonth]
            getMonthData(curYear, curMonth + 1)
        }

        nextMonthButton.setOnClickListener {
            if (curMonth == 11) {
                curMonth = 0
            } else {
                curMonth += 1
            }
            chartMonthTextViewButton.text = months[curMonth]
            getMonthData(curYear, curMonth + 1)
        }

        prevYearButton.setOnClickListener {
            curYear -= 1
            chartYearTextViewButton.text = curYear.toString()
            getMonthData(curYear, curMonth + 1)
        }

        nextYearButton.setOnClickListener {
            curYear += 1
            chartYearTextViewButton.text = curYear.toString()
            getMonthData(curYear, curMonth + 1)
        }
    }

    private fun getMonthData(year: Int, month: Int) {
        val pattern = "$year-$month.*".toRegex()
        chartItems.clear()
        var x = 1f

        balanceItems.forEach {
                item -> if (pattern.containsMatchIn(item.date)) {
            chartItems.add(
                BarEntry(x, item.balance.toFloat())
            )
        }
            x += 1
        }

        val barDataSet = BarDataSet(chartItems, "Label")
        val barData = BarData(barDataSet)
        chart.data = barData
        chart.invalidate()
    }
}
